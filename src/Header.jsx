import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Row, Container, Col} from 'react-bootstrap';
import './Style.css'
class Header extends Component{
   render(){
      return(
         <div className="header-wrapper d-flex align-items-center">
            <Container className="text-light">
               <Row noGutters>
               <Col lg={6} md={6} sm={6}>
                  <Link to="/">
                     <span className="text-light">mahesh</span>
                  </Link>
               </Col>
                  <Col lg={6} md={6} sm={6} className="d-flex justify-content-end">
                     <Link to="/Contact"><span className="text-light">Contact</span></Link>
                  </Col>
               </Row>
            </Container>
         </div>
      );
   }
}
export default Header;
export { Header };