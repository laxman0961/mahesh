import React, { Component } from 'react';
import { Button, Container, Card, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import './Style.css'
class Home extends Component{
   render(){
      return(
         <Container className='home-container'>
            <Card className="card-wrapper flex-row-reverse border-0">
               <Card.Img className="w-50" variant="bottom" src="https://dav87bg0vwosn.cloudfront.net/4/studio/assets/v1577006258680_2046279278/Screenshot_20191222-144123__01.jpg" />
               <Card.Body className="d-flex flex-column justify-content-center">
                  <Card.Title>I'm Mahesh</Card.Title>
                  <Card.Text>
                     Senior Quality Engineer on India
                  </Card.Text>
                  <Card.Title>General Info</Card.Title>
                  <Card.Text>
                     <Row>
                        <Col lg={6}>
                           <span>Date Of Birth</span>
                        </Col>
                        <Col lg={6}>
                           <span>XXXXXX</span>
                        </Col>
                     </Row>
                     <Row>
                        <Col lg={6}>
                           <span>Address</span>
                        </Col>
                        <Col lg={6}>
                           <span>XXXXXX</span>
                        </Col>
                     </Row>
                     <Row>
                        <Col lg={6}>
                           <span>E-mail</span>
                        </Col>
                        <Col lg={6}>
                           <span>XXXXXX</span>
                        </Col>
                        <Col lg={6}>
                           <span>Phone</span>
                        </Col>
                        <Col lg={6}>
                           <span>XXXXXX</span>
                        </Col>
                     </Row>
                  </Card.Text>
                  <Button variant="secondary w-50">
                     <Link to="/Contact"><span className="text-light">Contact</span></Link>
                  </Button>
               </Card.Body>
               </Card>
         </Container>
      );
   }
}
export default Home;
export { Home };