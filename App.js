import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { Home } from './src/Home.jsx'
import { Header } from './src/Header.jsx'
import { Contact } from './src/Contact.jsx'
class App extends Component{
   render(){
      return(
         <Router>
         <div>
            <Header />
            <Switch>
               <Route path="/" exact component={Home} />
               <Route path="/Contact" exact component={Contact} />
            </Switch>
         </div>
         </Router>
      );
   }
}
export default App;